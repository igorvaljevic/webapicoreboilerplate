﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WebApiCore.Exceptions;
using WebApiCore.Models.Outbound;

namespace WebApiCore.Middlewares
{
    public class ExceptionLogHandler
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        public ExceptionLogHandler(RequestDelegate next, ILogger<ExceptionLogHandler> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                GenerateErrorLog(ex);
                throw;
            }
        }

        private void GenerateErrorLog(Exception exception)
        {
            List<ResponseErrorDto> errorDetails;
            LogLevel logLevel;
            if (exception is ValidationException)
            {
                var validationException = (ValidationException)exception;
                errorDetails = validationException.ErrorCollection.Select(validationError => new ResponseErrorDto
                {
                    Message = validationError.DetailedMessage,
                    Field = validationError.Field,
                    ErrorCode = validationError.ErrorCode,
                    Type = "validation"
                }).ToList();
                logLevel = LogLevel.Warning;
            }
            else if (exception is ModelStateException)
            {
                var modelStateException = (ModelStateException)exception;
                errorDetails = modelStateException.ModelState.Select(modelStateError => new ResponseErrorDto
                {
                    Message = String.Join("; ", modelStateError.Value.Errors.Select(e => e.ErrorMessage)),
                    Field = modelStateError.Key,
                    Type = "modelStateValidation",
                }).ToList();
                logLevel = LogLevel.Error;
            }
            else if (exception is VerificationException)
            {
                var verificationException = (VerificationException)exception;
                errorDetails = verificationException.ErrorCollection.Select(verificationError => new ResponseErrorDto
                {
                    Message = verificationError.DetailedMessage,
                    Field = verificationError.Field,
                    ErrorCode = verificationError.ErrorCode,
                    Type = "verification"
                }).ToList();
                logLevel = LogLevel.Warning;
            }
            else
            {
                errorDetails = new List<ResponseErrorDto>();
                var nextException = exception;
                while (nextException != null)
                {
                    var error = new ResponseErrorDto
                    {
                        Message = exception.Message,
                        Type = "internalServerError",
                    };
                    errorDetails.Add(error);
                    nextException = nextException.InnerException;
                }
                logLevel = LogLevel.Critical;
            }
            var message = JsonConvert.SerializeObject(errorDetails);
            _logger.Log(logLevel, 0, message, exception, (logMessage, exc) => message);
        }
    }
}
