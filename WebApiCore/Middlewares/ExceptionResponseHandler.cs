﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApiCore.Exceptions;
using WebApiCore.Models.Outbound;

namespace WebApiCore.Middlewares
{
    public class ExceptionResponseHandler
    {
        private readonly RequestDelegate _next;
        private IHostingEnvironment _hostingEnvironment;
        public ExceptionResponseHandler(IHostingEnvironment hostingEnvironment,
            RequestDelegate next)
        {
            _hostingEnvironment = hostingEnvironment;
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await GenerateErrorResponse(context, ex);
            }
        }

        private async Task GenerateErrorResponse(HttpContext context, Exception exception)
        {
            List<ResponseErrorDto> errorDetails;
            HttpStatusCode statusCode;
            if (exception is ValidationException)
            {
                var validationException = (ValidationException)exception;
                errorDetails = validationException.ErrorCollection.Select(validationError => new ResponseErrorDto
                {
                    Message = validationError.DetailedMessage,
                    Field = validationError.Field,
                    ErrorCode = validationError.ErrorCode,
                    Type = "validation"
                }).ToList();
                statusCode = HttpStatusCode.BadRequest;
            }
            else if (exception is ModelStateException)
            {
                var modelStateException = (ModelStateException)exception;
                errorDetails = modelStateException.ModelState.Select(modelStateError => new ResponseErrorDto
                {
                    Message = String.Join("; ", modelStateError.Value.Errors.Select(e => e.ErrorMessage)),
                    Field = modelStateError.Key,
                    Type = "modelStateValidation",
                }).ToList();
                statusCode = HttpStatusCode.BadRequest;
            }
            else if (exception is VerificationException)
            {
                var verificationException = (VerificationException)exception;
                errorDetails = verificationException.ErrorCollection.Select(verificationError => new ResponseErrorDto
                {
                    Message = verificationError.DetailedMessage,
                    Field = verificationError.Field,
                    ErrorCode = verificationError.ErrorCode,
                    Type = "verification"
                }).ToList();
                statusCode = HttpStatusCode.BadRequest;
            }
            else
            {
                errorDetails = new List<ResponseErrorDto>();
                if (_hostingEnvironment.IsProduction())
                {
                    errorDetails.Add(new ResponseErrorDto
                    {
                        Message = "Internal Server Error. Please contact administrator for more information",
                        Type = "internalServerError",
                    });
                }
                else
                {
                    while (exception != null)
                    {
                        var error = new ResponseErrorDto
                        {
                            Message = exception.Message,
                            Type = "internalServerError",
                        };
                        errorDetails.Add(error);
                        exception = exception.InnerException;
                    }
                }
                statusCode = HttpStatusCode.InternalServerError;
            }

            var message = JsonConvert.SerializeObject(errorDetails);
            context.Response.StatusCode = (int)statusCode;
            await context.Response.WriteAsync(message);
        }
    }
}
