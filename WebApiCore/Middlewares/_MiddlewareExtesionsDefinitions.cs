﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace WebApiCore.Middlewares
{
    public static class _MiddlewareExtesionsDefinitions
    {
        public static IApplicationBuilder UseExceptionResponseHandler(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionResponseHandler>();
        }
        public static IApplicationBuilder UseExceptionLogHandler(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionLogHandler>();
        }
    }
}
