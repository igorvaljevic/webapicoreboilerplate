﻿namespace WebApiCore.Models
{
    public enum ErrorCode
    {
        CampaignNotFound = 1003,
        CampaignAdvertPlaceholderNotFound,
        InvalidSortField,
        InvalidFieldFilter,
        ArticlePlaceholdersNotFound,
        PositionIndexLargerThanNumberOfArticles,
        CampaignAdvertBodyNotFound,
        ModelStateBindingError
    }
}
