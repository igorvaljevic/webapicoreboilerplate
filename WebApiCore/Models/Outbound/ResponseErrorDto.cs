﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Models.Outbound
{
    public class ResponseErrorDto
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string Field { get; set; }
        public ErrorCode ErrorCode { get; set; }
    }
}
