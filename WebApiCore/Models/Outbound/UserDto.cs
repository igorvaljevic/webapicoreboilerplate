﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Models.Outbound
{
    public class UserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
