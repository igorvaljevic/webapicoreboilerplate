﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Exceptions
{
    public class VerificationException : Exception
    {
        private List<ErrorState> _errorCollection;
        public List<ErrorState> ErrorCollection
        {
            get { return _errorCollection; }
        }

        public VerificationException(List<ErrorState> errorCollection)
        {
            _errorCollection = errorCollection;
        }
    }
}
