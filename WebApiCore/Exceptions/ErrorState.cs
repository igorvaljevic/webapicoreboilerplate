﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiCore.Models;

namespace WebApiCore.Exceptions
{
    public class ErrorState
    {
        public ErrorCode ErrorCode { get; set; }
        public string DetailedMessage { get; set; }
        public string Field { get; set; }
    }
}
