﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Dynamic;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;
using WebApiCore.Models;
using WebApiCore.Models.Outbound;

namespace WebApiCore.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private IOptions<AppSettings> _appSettings;
        public UsersController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings;
        }

        /// <summary>
        /// Retrieves all users
        /// </summary>
        /// <remarks>Awesomeness!</remarks>
        /// <response code="200">Product created</response>
        [HttpGet]
        public async Task<IEnumerable<UserDto>> Get()
        {
            var users = await Task.Run(() => new List<UserDto> { new UserDto { FirstName = "Igor", LastName = "Valjevic" } });
            throw new NotImplementedException();
            return users;
        }

    }
}
